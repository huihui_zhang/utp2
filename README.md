## UML Testing Profile 2 (UTP 2) Specification Version 2.0-Beta

"In 2001, a working group at the OMG started developing a UML Profile dedicated to Model-based testing, called UML Testing Profile (UTP). It is a standardized language based on OMG�s Unified Modeling Language (UML) for designing, visualizing, specifying, analyzing, constructing, and documenting the artifacts commonly used in and required for various testing approaches, in particular model-based testing (MBT) approaches. UTP has the potential to assume the same important role for model-based testing approaches as UML assumes for model-driven system engineering."---OMG

The latest UML Testing Profile 2 (UTP 2) Specification is published in September 2017, i.e.,[OMG UTP 2.0-beta Specification](http://www.omg.org/spec/UTP/About-UTP/) 

---

## Current Implementaton

Current implementation of OMG UTP 2.0-beta Specification is basded on [IBM Rational Software Architect (RSA)](https://www.ibm.com/developerworks/downloads/r/architect/index.html), which is a standard RSA profile project and it includes the released profile and the corresponding libraries. 

The corresponding documentation of RSA UTP2.0 Profile can be found here: [online documentation of UTP 2.0-beta RSA](http://zen-tools.com/UTP2.0web/index.html).


---

## Setup Instructions 

RSA UTP2.0 Profile is a UML-based model project. Once you have downloaded the RSA project, you can easily setup and employ it by following steps:

    1. Unzip the RSA project file 'UTP Profile-RSA9.1.2.zip';
	2. Import this project into RSA;
	3. Create Model project;
	4. Add the 'UTP' profile as Applied Proflies in the "Details" option;
	5. Add Libraries (i.e. UTP Library) as Model Libraries in the "Details" option.

---

## Contact

 Shaukat Ali (shaukat@simula.no)
 
 Tao Yue (tao@simula.no)
 
 Man Zhang (manzhang@simula.no)
 
 Huihui Zhang (pkuzhhui@gmail.com) 